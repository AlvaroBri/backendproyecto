function calculaDC(entidad, oficina,cuenta) {
  var pesos = new Array();
  pesos[0] = 6;
  pesos[1] = 3;
  pesos[2] = 7;
  pesos[3] = 9;
  pesos[4] = 10;
  pesos[5] = 5;
  pesos[6] = 8;
  pesos[7] = 4;
  pesos[8] = 2;
  pesos[9] = 1;

  entidadInv = inv(entidad);
  oficinaInv = inv(oficina);
  cuentaInv = inv(cuenta);

  ofient = "" + oficinaInv + entidadInv
  suma =0;
  for(a=0;a<8;a++)
      suma += ofient.substr(a,1)*pesos[a];

  sumaCCC =0;
  for(a=0;a<10;a++)
      sumaCCC += cuentaInv.substr(a,1)*pesos[a];

  dc = 11 - (suma % 11)
  dcCcc = 11 - (sumaCCC % 11)

  if (dc == 10) dc = 1;
  if (dc == 11) dc = 0;
  if (dcCcc == 10) dcCcc = 1;
  if (dcCcc == 11) dcCcc = 0;

  return ""+dc + dcCcc;
}

function inv(item) {
  var ret = "";

  for (a=0;a<=item.length;a++)
      ret = ret + item.substr(item.length-a,1);

  return ret
}

function calculaIBANSpain(entidad,sucursal,cuenta) {
    var countryCode = "1428"; // Spain code

    var iban = entidad+sucursal;

    iban = "" + (iban % 97) + cuenta + cuenta.substring(0,2);
    iban = "" + (iban % 97) + cuenta.substring(2,cuenta.length) + countryCode + '00';

    modIban = iban % 97;
    ccIban = 98 - modIban;
            if(ccIban<10)
        ccIban = "0" + ccIban;

    return "ES" + ccIban;
}


function padLeftZeroes(number, length) {
    var formatString = '' + number;
    while (formatString.length < length) {
        formatString = '0' + formatString;
    }

    return formatString;
}

module.exports.calculaDC = calculaDC;
module.exports.calculaIBANSpain = calculaIBANSpain;
module.exports.padLeftZeroes = padLeftZeroes;
