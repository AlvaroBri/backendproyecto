/*Carga libreria request-json*/
const requestJson = require('request-json');

const baseNeutrinoURL = "https://neutrinoapi.com/";

function exchangeV1(req, res){
  console.log("POST /goliathnationalbank/v1/exchange");

  var currencyExchange = {
    "user_id": process.env.NEUTRINO_USER_ID,
    "api_key": process.env.NEUTRINO_API_KEY,
    "from_value": req.body.fromValue,
    "from_type": req.body.fromType,
    "to_type": req.body.toType
  }

  console.log("currencyExchange.user_id: " + currencyExchange.user_id);
  console.log("currencyExchange.api_key: " + currencyExchange.api_key);
  console.log("currencyExchange.from_value: " + currencyExchange.from_value);
  console.log("currencyExchange.from_type: " + currencyExchange.from_type);
  console.log("currencyExchange.to_type: " + currencyExchange.to_type);

  //Crea el cliente http indicando la url base de las peticiones
  var httpClient = requestJson.createClient(baseNeutrinoURL);
  console.log("Client created");

  httpClient.post("convert", currencyExchange,
    function(err, resMLab, body){
      if(err){
        console.log("Error conversion: "  + err);
        res.status(500).send({"msg" : "Error en la conversión"});
      } else{
        console.log("Conversión correcta");
        res.send(body);
      }
    }
  )
}

module.exports.exchangeV1 = exchangeV1;
