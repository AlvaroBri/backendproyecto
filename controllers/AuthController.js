const crypt = require('../crypt');

/*Carga libreria request-json*/
const requestJson = require('request-json');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechuabi10ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

//Login
function loginV1(req,res){
  console.log("POST /goliathnationalbank/v2/login");

  console.log("email: " + req.body.email);
  console.log("password: " + req.body.password);

  var query = 'q={"email": "' + req.body.email + '"}';

  //Crea el cliente http indicando la url base de las peticiones
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body){
      if(err){
        console.log("Login incorrecto" + err);
        res.status(401);
        res.send({"msg" : "Login incorrecto"});
      }else{
        if(body.length > 0){
          console.log("Email encontrado");
          console.log(body[0].password);
          if (crypt.checkPassword(req.body.password, body[0].password)){
            console.log("Password coincide");
            var id = body[0].id;
            query = 'q={"id":' + id + '}';
            var putBody = '{"$set":{"logged":true}}';
            httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
              function(err2, resMLab2, body2){
                if(err2){
                  console.log("Login incorrecto" + err2);
                  res.status(401);
                  res.send({"msg" : "Login incorrecto"});
                }else{
                  console.log("Client logado");
                  res.send({"msg" : "Login correcto", "idUsuario" : id});
                }
              }
            )
          }else{
            console.log("Password no coincide");
            res.status(401);
            res.send({"msg" : "Login incorrecto"});
          }
        }else{
          console.log("Email no encontrado");
          res.status(401);
          res.send({"msg" : "Login incorrecto"});
        }
      }
    }
  )
}

//Logout
function logoutV1(req, res){
  console.log("POST /goliathnationalbank/v2/logout");
  console.log("id es " + req.params.id);

  var query = 'q={"id": ' + req.params.id + '}';
  var putBody = '{"$unset":{"logged":""}}';

  //Crea el cliente http indicando la url base de las peticiones
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body){
      if (err){
        console.log("Error en la consulta" + err);
        res.status(403);
        res.send({"msg" : "Logout incorrecto"});
      }else{
        if(body.length > 0){
          console.log("Id encontrado");
          console.log(body[0].logged);
          if (body[0].logged){
            console.log("Esta logado");
            var id = body[0].id;
            httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
              function(err2, resMLab2, body2){
                if(err){
                  console.log("Error en el logout" + err2);
                  res.status(403);
                  res.send({"msg" : "Logout incorrecto"});
                }else{
                  console.log("Client deslogado");
                  res.send({"msg" : "Logout correcto", "idUsuario" : id});
                }
              }
            )
          }else{
            console.log("No esta logado");
            res.status(403);
            res.send({"msg" : "Logout incorrecto"});
          }
        }else{
          console.log("Id no encontrado");
          res.status(403);
          res.send({"msg" : "Logout incorrecto"});
        }
      }
    }
  )
}

module.exports.loginV1 = loginV1;
module.exports.logoutV1 = logoutV1;
