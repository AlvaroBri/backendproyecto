const io = require('../io.js');
const crypt = require('../crypt');

/*Carga libreria request-json*/
const requestJson = require('request-json');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechuabi10ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function createTransactionV1(req, res){
  console.log("POST /goliathnationalbank/v1/transaction");

  console.log("request createTransactionV1");
  console.log(req.body.iban);
  console.log(req.body.type);
  console.log(Number(req.body.amount));
  console.log(req.body.description);

  //Contador incremental para el calculo del balance total
  var counter ={
     "findAndModify":"account",
      "query":
        {"IBAN":req.body.iban},
      "update":
        {"$inc":{"balance":req.body.amount}},
      "new":true
  }

  //Crea el cliente http indicando la url base de las peticiones
  var httpClient1 = requestJson.createClient("https://api.mlab.com/api/1/databases/apitechuabi10ed/");
  console.log("Client created");

  httpClient1.post("runCommand?" + mLabAPIKey, counter,
    function(err, resMLab, body){
      if(err){
        console.log("Error actualizando el balance: " + err);
        res.status(500).send({"msg" : "Error actualizando el balance"});
      } else{
        console.log("Contador incrementado: " + body.value.balance);

        var newTransaction = {
          "IBAN": req.body.iban,
          "accountAmount": body.value.balance,
          "type": req.body.type,
          "amount": req.body.amount,
          "description": req.body.description,
          "date": new Date()
        };
        console.log('date:' + newTransaction.date);

        //Crea el cliente http indicando la url base de las peticiones
        var httpClient2 = requestJson.createClient(baseMLabURL);

        console.log("Client created");
        httpClient2.post("transaction?" + mLabAPIKey, newTransaction,
          function(err2, resMLab2, body2){
            if(err2){
              res.status(500).send({"msg" : "Error creando la transacción"});
            }else{
              console.log("Transaccion creada en Mlab");
              res.status(201).send({"msg" : "Transaccion creada", "productId": req.body.iban});
            }
          }
        )
      }
    }
  )
}


function getByIbanV1(req, res){
  console.log("GET /goliathnationalbank/v1/transaction/:iban");

  var iban = req.params.iban;
  var query = 'q={"IBAN": "' + iban + '"}';
  console.log("La consulta es " + query);

  //Crea el cliente http indicando la url base de las peticiones
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");
  var sort = 's={"date": -1}';
  httpClient.get("transaction?" + query + "&" + sort + "&" + mLabAPIKey,
    function(err, resMLab, body){
      if(err){
        var response = {
          "msg" : "Error obteniendo transacciones"
        }
        res.status(500);
      }else{
        if(body.length > 0){
          var response = body;
        }else{
          var response =  {
            "msg" : "Transacciones no encontradas"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  )
}


module.exports.createTransactionV1 = createTransactionV1;
module.exports.getByIbanV1 = getByIbanV1;
