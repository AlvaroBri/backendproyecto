const crypt = require('../crypt');

/*Carga libreria request-json*/
const requestJson = require('request-json');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechuabi10ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

//Metodo para la consulta de usuarios
function getUsersV1 (req, res){
  console.log("GET /goliathnationalbank/v2/users");

  //Crea el cliente http indicando la url base de las peticiones
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  httpClient.get("user?" + mLabAPIKey,
    function(err, resMLab, body){
      var response = !err ? body : {
        "msg" : "Error obteniendo usuarios"
      }
      res.send(response);
    }
  )
}

//Metodo para la consulta de usuarios por id
function getUserByIdV1(req, res){
  console.log("GET /goliathnationalbank/v1/users/:id");

  var id = req.params.id;
  var query = 'q={"id":' + id + '}';
  console.log("La consulta es " + query);

  //Crea el cliente http indicando la url base de las peticiones
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body){
      if(err){
        var response = {
          "msg" : "Error obteniendo usuarios"
        }
        res.status(500);
      }else{
        if(body.length > 0){
          var response = body[0];
        }else{
          var response =  {
            "msg" : "Usuario no encontrado"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  )
}

//Metodo para la actualización de un usuario
function putUserV1(req, res){
  console.log("GET /goliathnationalbank/v1/users/:id");

  var id = req.params.id;
  var query = 'q={"id":' + id + '}';
  console.log("La consulta es " + query);

  console.log("request putUserV1");
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);
  console.log(req.body.password);
  console.log(req.body.newPassword);

  var password;
  if (req.body.newPassword == null || req.body.newPassword == ''){
    console.log("Password sin cambio");
    password = req.body.password;
  }else{
    console.log("Password diferente");
    password = crypt.hash(req.body.newPassword);
  }

  var putUser = {
    "id": Number(req.params.id),
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,
    "password": password,
    "logged": true
  }

  //Crea el cliente http indicando la url base de las peticiones
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  httpClient.put("user?" + query + "&" + mLabAPIKey, putUser,
    function(err, resMLab, body){
      if(err){
        console.log("Error actualizando el usuario");
        res.status(500).send({"msg" : "Error actualizando el usuario"});
      }else{
        console.log("Usuario actualizado");
        res.send({"msg" : "Usuario actualizado"});
      }
    }
  )
}

//Metodo para la creación de usuarios
function createUserV1(req, res){
  console.log("POST /goliathnationalbank/v1/users");

  console.log("request createUserV1");
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);
  console.log(req.body.password);

  //Contador incremental para el calculo del id del usuario.
  var counter ={
	   "findAndModify":"counter",
	    "query":
		    {"_id":"userid"},
	    "update":
		    {"$inc":{"sequence_value":1}},
	    "new":true
  }

  //Crea el cliente http indicando la url base de las peticiones
  var httpClient = requestJson.createClient("https://api.mlab.com/api/1/databases/apitechuabi10ed/");
  console.log("Client created");

  httpClient.post("runCommand?" + mLabAPIKey, counter,
    function(err, resMLab, body){
      if (err){
        console.log("Error dando de alta usuario");
        res.status(500).send({"msg" : "Error dando de alta usuario"});
      }else{
        console.log("Contador incrementado");
        console.log("Contador userId " + body.value.sequence_value);

        var newUser = {
          "id": body.value.sequence_value,
          "first_name": req.body.first_name,
          "last_name": req.body.last_name,
          "email": req.body.email,
          "password": crypt.hash(req.body.password)
        }

        //Crea el cliente http indicando la url base de las peticiones
        var httpClient = requestJson.createClient(baseMLabURL);
        console.log("Client created");

        httpClient.post("user?" + mLabAPIKey, newUser,
          function(err2, resMLab2, body2){
            if(err2){
              console.log("Error dando de alta el usuario");
              res.status(500).send({"msg" : "Error dando de alta el usuario"});
            }else{
              console.log("Usuario creado en Mlab");
              res.status(201).send({"msg" : "Usuario guardado"});
            }
          }
        )
      }
    }
  )
}

module.exports.getUsersV1 = getUsersV1;
module.exports.getUserByIdV1 = getUserByIdV1;
module.exports.putUserV1 = putUserV1;
module.exports.createUserV1 = createUserV1;
