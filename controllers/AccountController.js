const io = require('../io.js');
const crypt = require('../crypt');

/*Carga libreria request-json*/
const requestJson = require('request-json');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechuabi10ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;


function createAccountV1(req, res){
  console.log("POST /goliathnationalbank/v1/accounts");

  console.log("request createAccountV1");
  console.log(req.body.userId);

  var entidad = "0182";
  var oficina = "1128";

  //Contador incremental para el calculo del id del usuario.
  var counter ={
	   "findAndModify":"counter",
	    "query":
		    {"_id":"accountid"},
	    "update":
		    {"$inc":{"sequence_value":1}},
	    "new":true
  }

  //Crea el cliente http indicando la url base de las peticiones
  var httpClient = requestJson.createClient("https://api.mlab.com/api/1/databases/apitechuabi10ed/");
  console.log("Client created");

  httpClient.post("runCommand?" + mLabAPIKey, counter,
    function(err, resMLab, body){
      if(err){
        console.log("Error creando la cuenta: " + err);
        res.status(500).send({"msg" : "Error creando la cuenta"});
      }else{
        console.log("Contador incrementado");
        console.log("Contador accountid " + body.value.sequence_value);

        var account = io.padLeftZeroes(body.value.sequence_value,10);
        console.log("numberAccount "+ account);
        var dc = io.calculaDC(entidad,oficina,account);
        console.log("DC " + dc);
        var ibanDC = io.calculaIBANSpain(entidad,oficina,account);
        console.log("IBAN " + ibanDC);

        var ibanNumber =ibanDC + entidad + oficina + dc + account;
        console.log("ibanNumber " + ibanNumber);

        var newAccount = {
          "IBAN": ibanNumber,
          "idUsuario": req.body.userId,
          "balance": 1000
        }

        //Crea el cliente http indicando la url base de las peticiones
        var httpClient = requestJson.createClient(baseMLabURL);
        console.log("Client created");

        httpClient.post("account?" + mLabAPIKey, newAccount,
          function(err2, resMLab2, body2){
            if(err2){
              console.log("Error creando la cuenta: " + err);
              res.status(500).send({"msg" : "Error creando la cuenta"});
            } else{
              console.log("Cuenta creada en Mlab");

              var inicialTransaction = {
                "IBAN": ibanNumber,
                "accountAmount": 1000,
                "type": "I",
                "amount": 1000,
                "description": "Abono de Nómina",
                "date": new Date()
              };

              console.log("Client created");
              httpClient.post("transaction?" + mLabAPIKey, inicialTransaction,
                function(err3, resMLab3, body3){
                  if(err3){
                    console.log("Error creando la transacción inicial: " + err);
                    res.status(500).send({"msg" : "Error creando la transacción inicial"});
                  }else{
                    console.log("Transaccion creada en Mlab");
                    res.status(201).send({"msg" : "Cuenta creada", "idUsuario": req.body.userId});
                  }
                }
              )
            }
          }
        )
      }
    }
  )
}

function getByUserIdV1(req, res){
  console.log("GET /goliathnationalbank/v1/accounts/:id");

  var id = req.params.id;
  var query = 'q={"idUsuario":' + id + '}';
  console.log("La consulta es " + query);

  //Crea el cliente http indicando la url base de las peticiones
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  httpClient.get("account?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body){
      if(err){
        var response = {
          "msg" : "Error obteniendo cuentas"
        }
        res.status(500);
      }else{
        if(body.length > 0){
          var response = body;
        }else{
          var response =  {
            "msg" : "Cuentas no encontradas"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  )
}

function deleteAccountV1(req, res){
  console.log("DELETE /goliathnationalbank/v1/delete/:id");

  var id = req.params.id;
  var query = 'q={"IBAN": "' + id + '"}';
  console.log("La consulta es " + query);

  //Crea el cliente http indicando la url base de las peticiones
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  httpClient.put("account?" + query + "&" + mLabAPIKey, [],
    function(err, resMLab, body){
      if(err){
        console.log("Error borrando la cuenta" + err);
        res.status(500).send({"msg" : "Error borrando la cuenta"});
      }else{
        httpClient.put("transaction?" + query + "&" + mLabAPIKey, [],
          function(err2, resMLab2, body2){
            if(err2){
              console.log("Error borrando las transacciones" + err2);
              res.status(500).send({"msg" : "Error borrando las transacciones"});
            }else{
              res.send({"msg" : "Cuenta borrada"});
            }
          }
        )
      }
    }
  )
}

module.exports.createAccountV1 = createAccountV1;
module.exports.getByUserIdV1 = getByUserIdV1;deleteAccountV1
module.exports.deleteAccountV1 = deleteAccountV1;
