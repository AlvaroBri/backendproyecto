//Carga configuración por defecto gestión de variables de entorno
require('dotenv').config();

/*Carga libreria express*/
const express = require('express');
/*Inicia el framework express*/
const app = express();

/*Inicializa el puerto si tiene valor y sino lo tiene, carga por defecto el puerto 3000*/
const port = process.env.PORT || 3000;
//Carga controlador de usuarios para poder usar sus funciones
const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');
const accountController = require('./controllers/AccountController');
const transactionController = require('./controllers/TransactionController');
const currencyController = require('./controllers/CurrencyController');

var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
 res.set("Access-Control-Allow-Headers", "Content-Type");

 next();
}

//Procesado del body cuando es json
app.use(express.json());
//Habilida el crossdomain
app.use(enableCORS);
/*Inicializa el servidor escuchando en el puerto*/
app.listen(port);

console.log("API escuchando en el puerto " + port);

app.get('/goliathnationalbank/v1/users', userController.getUsersV1);
app.get('/goliathnationalbank/v1/users/:id', userController.getUserByIdV1);
app.put('/goliathnationalbank/v1/users/:id', userController.putUserV1);
app.post('/goliathnationalbank/v1/users', userController.createUserV1);


app.post('/goliathnationalbank/v1/login', authController.loginV1);
app.post('/goliathnationalbank/v1/logout/:id', authController.logoutV1);

app.post('/goliathnationalbank/v1/accounts', accountController.createAccountV1);
app.get('/goliathnationalbank/v1/accounts/:id', accountController.getByUserIdV1);
app.delete('/goliathnationalbank/v1/accounts/:id', accountController.deleteAccountV1);

app.post('/goliathnationalbank/v1/transaction', transactionController.createTransactionV1);
app.get('/goliathnationalbank/v1/transaction/:iban', transactionController.getByIbanV1);

app.post('/goliathnationalbank/v1/exchange', currencyController.exchangeV1);
